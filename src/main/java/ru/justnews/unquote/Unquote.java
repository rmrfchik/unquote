package ru.justnews.unquote;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Unquote {

    public static final String DEFAULT_SEP = " \t";
    public static final String DEFAULT_N_SEP = "|";
    public static final String DEFAULT_QUOTE = "\"";

    public static void main(String... args) {
        String sep = DEFAULT_SEP;
        String nsep = DEFAULT_N_SEP;
        String quote = DEFAULT_QUOTE;

        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-h":
                    help();
                    System.exit(0);
                    break;
                case "-f":
                    sep = arg("-f", i + 1, args);
                    i++;
                    break;
                case "-t":
                    nsep = arg("-d", i + 1, args);
                    i++;
                    break;
                case "-s":
                    quote = "'";
                    break;
                case "-d":
                    quote = "\"";
                    break;
                case "-b":
                    quote = "'\"";
                    break;
                case "-q":
                    quote = arg("-q", i + 1, args);
                    i++;
                    break;
                default:
                    System.err.println("Unknown option " + args[i]);
                    help();
                    System.exit(1);

            }
        }
        process(sep, nsep, quote);
    }

    public static String arg(String arg, int idx, String... args) {
        if (idx >= args.length) {
            System.err.println(arg + " requires argument");
            System.exit(1);
        }
        return args[idx];
    }

    public static void help() {
        System.out.println("Change field separator and unquote quotted strings");
        System.out.println("Usage: unquote [-f separator] [-t new_separator] [-s|-d|-b|-q quote] [-h]");
        System.out.println(" -f separator : use given source separator to break fields, default space and tab \" \\t\"");
        System.out.println(" -t new_separator: separate fields when output, default |");
        System.out.println(" -s : use single quote \"'\"");
        System.out.println(" -d : use double quote \"\"\"");
        System.out.println(" -b : use both single and double quotes");
        System.out.println(" -q quote : use given character as quote");
    }

    public static List<String> breakString(final String line, final String sep, final String nsep, final String quote) {
        List<String> lines = new ArrayList<>();
        StringBuilder bline = new StringBuilder();
        boolean delim = true; // eat all leading delimiters
        boolean inQuote = false;
        char startQuote = ' ';
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            boolean quoteFound = quote.indexOf(c) >= 0;
            if (!inQuote && quoteFound) {
                startQuote = c;
                inQuote = true;
            } else if (inQuote && startQuote == c) {
                lines.add(bline.toString());
                bline = new StringBuilder();
                inQuote = false;
                delim = true; // eat all following delimiters
            } else if (!inQuote && sep.indexOf(c) >= 0) {
                if (!delim) {
                    lines.add(bline.toString());
                    bline = new StringBuilder();
                }
                delim = true;
            } else {
                bline.append(c);
                delim = false;
            }
        }
        if (bline.length() > 0) {
            lines.add(bline.toString());
        }
        return lines;
    }

    public static void process(final String sep, final String nsep, final String quote) {
        new BufferedReader(new InputStreamReader(System.in)).lines()
                .map(line -> breakString(line, sep, nsep, quote))
                .map(fields -> join(fields, nsep))
                .forEach(System.out::println);
    }

    public static String join(final List<String> fields, final String nsep) {
        return String.join(nsep, fields);
    }
}
