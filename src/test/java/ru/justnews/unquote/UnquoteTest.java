/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.justnews.unquote;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author paul
 */
public class UnquoteTest {

    public UnquoteTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of arg method, of class Unquote.
     */
    @Test
    public void testArg() {
        System.out.println("arg");
        String arg = "a";
        int idx = 1;
        String[] args = {"arg", "param"};
        String expResult = "param";
        String result = Unquote.arg(arg, idx, args);
        assertEquals(expResult, result);
    }

    /**
     * Test of breakString method, of class Unquote.
     */
    @Test
    public void testBreakString() {
        String line = "abc def bah";
        List<String> expResult = Arrays.asList(new String[]{"abc", "def", "bah"});
        List<String> result = Unquote.breakString(line, Unquote.DEFAULT_SEP, Unquote.DEFAULT_N_SEP, Unquote.DEFAULT_QUOTE);
        assertEquals(expResult, result);

        line = "abc  def bah"; // double delim
        expResult = Arrays.asList(new String[]{"abc", "def", "bah"});
        result = Unquote.breakString(line, Unquote.DEFAULT_SEP, Unquote.DEFAULT_N_SEP, Unquote.DEFAULT_QUOTE);
        assertEquals(expResult, result);

        line = "abc  def bah "; //trailing delim
        expResult = Arrays.asList(new String[]{"abc", "def", "bah"});
        result = Unquote.breakString(line, Unquote.DEFAULT_SEP, Unquote.DEFAULT_N_SEP, Unquote.DEFAULT_QUOTE);
        assertEquals(expResult, result);

        line = "  abc def bah"; //leading delim
        expResult = Arrays.asList(new String[]{"abc", "def", "bah"});
        result = Unquote.breakString(line, Unquote.DEFAULT_SEP, Unquote.DEFAULT_N_SEP, Unquote.DEFAULT_QUOTE);
        assertEquals(expResult, result);

        line = "abc \"def bah\" pew"; // quoted
        expResult = Arrays.asList(new String[]{"abc", "def bah", "pew"});
        result = Unquote.breakString(line, Unquote.DEFAULT_SEP, Unquote.DEFAULT_N_SEP, Unquote.DEFAULT_QUOTE);
        assertEquals(expResult, result);

        line = "abc \"def bah\" pew \"traling"; // no closing quote
        expResult = Arrays.asList(new String[]{"abc", "def bah", "pew", "traling"});
        result = Unquote.breakString(line, Unquote.DEFAULT_SEP, Unquote.DEFAULT_N_SEP, Unquote.DEFAULT_QUOTE);
        assertEquals(expResult, result);

        line = "abc \'def bah\'pew \'traling"; // no closing quote, single quote
        expResult = Arrays.asList(new String[]{"abc", "def bah", "pew", "traling"});
        result = Unquote.breakString(line, Unquote.DEFAULT_SEP, Unquote.DEFAULT_N_SEP, "'");
        assertEquals(expResult, result);

        
    }

}
